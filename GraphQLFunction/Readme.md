# Project description
The purpose of the project was to show case the use of the GraphQL technology in combination with Amazon Neptune and PostgreSQL / Aurora
queries through Hibernate).

Initially I also wanted to make a quering Lambda on top (which sort of can be figured out from the name of the repo), but afterwards
I turned down the idea, because it would make the project too big for a show case. So in that respect I am sorry for the
misleading repo name.

## Project contents
The project contains the next elements:
1. Javalin web server. I purposely didn't use spring boot or standalone Tomcat, not to shift away focus from the stuff that is
   important.
2. GraphQL schema, instantiated in **SchemaWiring.java** and is contained in _resources/objects.graphqls_.
3. Amazon Neptune, migration into which occurs in **DBMigrate.java** and instantiation is in **NeptuneClusterInstance.java**.
4. Amazon Aurora, migration into which occurs in **DBMigrate.java**, which is used through Hibernate's Element Managers.
   Hibernate-related sessings are contained in _resources/META-INF/persistence.xml_.
5. Object types are kept in _model/graph_ - for the objects in the GraphDB and in _model/relational_ - for the objects
that live in the relational DB.
6. Objects from the graph DB are populated using Java Reflection API and that occurs in **PojoWriter.java**.
7. Objects from the relational DB are populated by the means of Hibernate.
8. _resources/SFSRootCAG2.pem_ contains a public Amazon key that we use to connect to AWS Neptune with SSL.
9. Amazon DB instances are on my private AWS account. You will need your own. Good news - you can use FreeTier.
10. Fields queried through GraphQL are searched in graph objects, but should also be in the respective POJOs, otherwise
    it won't be possible to populate and return them.

### About author
This code is developed by Lev Vanyan (lev.vanyan@forthreal.com)