package com.forthreal.application;

import com.forthreal.application.classes.OverrideThrowChecking;
import com.forthreal.application.classes.api.Api;
import com.forthreal.application.classes.db.AuroraEntityManagerInstance;
import com.forthreal.application.classes.db.NeptuneClusterInstance;
import com.forthreal.application.classes.migrate.DBMigrate;
import com.forthreal.application.services.SchemaWiring;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import io.javalin.core.JavalinConfig;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.eclipse.jetty.server.Server;
import io.javalin.Javalin;
import org.eclipse.jetty.server.ServerConnector;
import org.javatuples.Pair;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.BufferedInputStream;
import java.net.URL;


public class ServiceLauncher
{
    private static GraphQLSchema graphQLSchema = null;
    private static GraphQL graphQL = null;

    private static
        Pair<Boolean, GraphQL>
        setupGraphQL
            (
              NeptuneClusterInstance instance,
              EntityManagerFactory entityManagerFactory,
              String schemaFile
            )
    {
        try
        {

            /* read the schema from file */
            URL url = instance.getClass().getClassLoader().getResource( schemaFile );

            BufferedInputStream stream = new BufferedInputStream( url.openStream() );

            if( stream.available() <= 0 )
            {
                System.err.println("Failed to read from  " + url.getPath() );
            }

            String schema = new String( stream.readAllBytes() );

            stream.close();

            /* set up graph QL schema*/
            TypeDefinitionRegistry registry = new SchemaParser().parse( schema );


            /* tether the schema types to the code */
            RuntimeWiring.Builder builder =
                    SchemaWiring.setupWiring
                       (
                         instance,
                         entityManagerFactory,
                         RuntimeWiring.newRuntimeWiring()
                       );

            RuntimeWiring wiring = builder.build();

            graphQLSchema = new SchemaGenerator().makeExecutableSchema( registry, wiring );

            graphQL = GraphQL.newGraphQL( graphQLSchema ).build();

            return Pair.with(true, graphQL);
        }
        catch(Exception exc)
        {
            exc.printStackTrace();

            OverrideThrowChecking.doThrow( exc );
        }

        return Pair.with(false, null);
    }


    public static void main(String[] args)
    {
        boolean hasLaunched = false;
        int buildingServicePort = 8081;

        /* we want to hook up on all interfaces */
        Server webServer = new Server();

        ServerConnector connector = new ServerConnector( webServer );
        connector.setHost( "0.0.0.0" );
        connector.setPort( buildingServicePort );

        webServer.setConnectors( new ServerConnector[] { connector } );

        JavalinConfig config = new JavalinConfig();

        config.server( () -> webServer );

        NeptuneClusterInstance instance =
                new NeptuneClusterInstance
                        (
                        "testdb-cluster.cluster-civvkbvrtpcp.eu-central-1.neptune.amazonaws.com",
                        8182,
                         "SFSRootCAG2.pem"
                        );
        /* AWS Neptune instance */
        Cluster cluster = instance.getCluster();
        /* AuroraDB instance, through Hibernate */
        EntityManager manager = AuroraEntityManagerInstance.getInstance();

        if( ( cluster != null ) && ( manager != null ) )
        {
            /* check if the instance is, try to set up */
            Pair<Boolean, GraphQL> pGraphQL =
                    setupGraphQL
                      (
                        instance,
                        AuroraEntityManagerInstance.getFactory(),
                        "objects.graphqls"
                      );

            if( pGraphQL.getValue0()  == true )
            {
                DBMigrate.doInitialGraphDBMigration( cluster );
                DBMigrate.doInitialRelationalDBMigration( manager );

                Javalin application = Javalin.create ( cfg -> { cfg = config; } );

                Api.create( graphQL );

                System.out.println("Starting service on buildingServicePort " + buildingServicePort);
                application.post( "/graphql", Api.getHandler() );

                application.start( buildingServicePort );

                hasLaunched = true;
            }
        }

        if( hasLaunched == false )
        {
            System.err.println("Launch failed");
        }

    }

}
