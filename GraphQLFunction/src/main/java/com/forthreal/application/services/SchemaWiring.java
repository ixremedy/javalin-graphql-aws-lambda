package com.forthreal.application.services;

import com.forthreal.application.classes.db.NeptuneClusterInstance;
import com.forthreal.application.classes.model.graph.*;
import com.forthreal.application.classes.retrieval.DataFunctionRetriever;
import com.forthreal.application.classes.retrieval.DataRetrieverMulti;
import com.forthreal.application.classes.retrieval.DataRetrieverSingle;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import javax.persistence.EntityManagerFactory;
import java.util.List;

public class SchemaWiring
{
    public static RuntimeWiring.Builder setupWiring
            (
              NeptuneClusterInstance clusterInstance,
              EntityManagerFactory entityManagerFactory,
              RuntimeWiring.Builder builder
            )
    {
        return
                builder
                        .type(
                                "Query",
                                typeWiring ->
                                  typeWiring
                                        .dataFetcher
                                           (
                                              "getPageByURL",
                                              new DataFetcher()
                                              {
                                                   @Override
                                                   public Page get(DataFetchingEnvironment environment)
                                                   {
                                                       return DataFunctionRetriever.get
                                                               (
                                                                   clusterInstance.getCluster(),
                                                                   entityManagerFactory.createEntityManager(),
                                                                   environment,
                                                                   String.class,
                                                                   Page.class,
                                                                  "Page",
                                                                  "url"
                                                               );
                                                  }
                                              }
                                           )
                                        .dataFetcher
                                           (
                                            "getViewById",
                                             new DataFetcher()
                                             {
                                                  @Override
                                                  public View get(DataFetchingEnvironment environment)
                                                  {
                                                       return DataFunctionRetriever.get
                                                               (
                                                                   clusterInstance.getCluster(),
                                                                   entityManagerFactory.createEntityManager(),
                                                                   environment,
                                                                   String.class,
                                                                   View.class,
                                                                  "View",
                                                                  "id"
                                                               );
                                                  }
                                             }
                                          )
                                        .dataFetcher
                                          (
                                                 "getInterestAreaById",
                                                 new DataFetcher()
                                                 {
                                                       @Override
                                                       public InterestArea get(DataFetchingEnvironment environment)
                                                       {
                                                          return DataFunctionRetriever.get
                                                                  (
                                                                     clusterInstance.getCluster(),
                                                                     entityManagerFactory.createEntityManager(),
                                                                     environment,
                                                                     String.class,
                                                                     InterestArea.class,
                                                                    "InterestArea",
                                                                    "id"
                                                                  );
                                                       }
                                                 }
                                          )

                            )

                        .type(
                                "Page",
                                typeWiring ->
                                        typeWiring
                                           .dataFetcher
                                                (
                                                        "interestArea",
                                                        new DataFetcher()
                                                        {

                                                            @Override
                                                            public InterestArea get(DataFetchingEnvironment environment)
                                                            {

                                                                return (InterestArea) DataFunctionRetriever.get
                                                                          (
                                                                              clusterInstance.getCluster(),
                                                                              entityManagerFactory.createEntityManager(),
                                                                              environment,
                                                                              String.class,
                                                                              InterestArea.class,
                                                                              "InterestArea",
                                                                              "id"
                                                                          );
                                                            }
                                                        }
                                                )
                                          .dataFetcher
                                                (
                                                        "view",
                                                        new DataFetcher()
                                                        {
                                                            @Override
                                                            public View get(DataFetchingEnvironment environment)
                                                            {
                                                                return (View) DataFunctionRetriever.get
                                                                        (
                                                                            clusterInstance.getCluster(),
                                                                            entityManagerFactory.createEntityManager(),
                                                                            environment,
                                                                            String.class,
                                                                            View.class,
                                                                            "View",
                                                                            "id"
                                                                        );
                                                            }
                                                        }
                                                )
                            )
                        .type(
                                "View",
                                 typeWiring ->
                                   typeWiring
                                        .dataFetcher
                                                (
                                                        "columns",
                                                        new DataFetcher()
                                                        {
                                                            @Override
                                                            public List<Column> get(DataFetchingEnvironment environment)
                                                            {
                                                                return DataRetrieverMulti.<Column>get(
                                                                        clusterInstance.getCluster(),
                                                                        entityManagerFactory.createEntityManager(),
                                                                        environment,
                                                                        Column.class,
                                                                        "Column"
                                                                    );
                                                            }
                                                        }
                                                )
                            )
                        .type(
                                "VisualFeature",
                                typeWiring ->
                                  typeWiring
                                        .dataFetcher
                                                (
                                                        "dataSources",
                                                        new DataFetcher()
                                                        {
                                                            @Override
                                                            public List<DataSource> get(DataFetchingEnvironment environment)
                                                            {
                                                                return DataRetrieverMulti.<DataSource>get(
                                                                        clusterInstance.getCluster(),
                                                                        entityManagerFactory.createEntityManager(),
                                                                        environment,
                                                                        DataSource.class,
                                                                        "DataSource"
                                                                );
                                                            }
                                                        }
                                                )
                            )
                        .type(
                                "DataSource",
                                typeWiring ->
                                  typeWiring
                                        .dataFetcher
                                                (
                                                        "column",
                                                        new DataFetcher()
                                                        {
                                                            @Override
                                                            public Column get(DataFetchingEnvironment environment)
                                                            {
                                                                return (Column) DataFunctionRetriever.get
                                                                        (
                                                                            clusterInstance.getCluster(),
                                                                            entityManagerFactory.createEntityManager(),
                                                                            environment,
                                                                            String.class,
                                                                            Column.class,
                                                                            "Column",
                                                                            "id"
                                                                        );
                                                            }
                                                        }
                                                )
                                        .dataFetcher
                                                (
                                                        "visualFeature",
                                                        new DataFetcher()
                                                        {
                                                            @Override
                                                            public VisualFeature get(DataFetchingEnvironment environment)
                                                            {
                                                                return (VisualFeature) DataFunctionRetriever.get
                                                                        (
                                                                            clusterInstance.getCluster(),
                                                                            entityManagerFactory.createEntityManager(),
                                                                            environment,
                                                                            String.class,
                                                                            VisualFeature.class,
                                                                            "VisualFeature",
                                                                            "id"
                                                                        );
                                                            }
                                                        }
                                                )
                            )
                        .type(
                                "Column",
                                typeWiring ->
                                  typeWiring
                                        .dataFetcher
                                                (
                                                        "visualFeatures",
                                                        new DataFetcher()
                                                        {
                                                            @Override
                                                            public List<VisualFeature> get(DataFetchingEnvironment environment)
                                                            {
                                                                return DataRetrieverMulti.<VisualFeature>get(
                                                                        clusterInstance.getCluster(),
                                                                        entityManagerFactory.createEntityManager(),
                                                                        environment,
                                                                        VisualFeature.class,
                                                                        "VisualFeature"
                                                                );
                                                            }
                                                        }
                                                )
                                        .dataFetcher
                                                (
                                                        "dataSources",
                                                        new DataFetcher()
                                                        {
                                                            @Override
                                                            public List<DataSource> get(DataFetchingEnvironment environment)
                                                            {
                                                                return DataRetrieverMulti.<DataSource>get(
                                                                        clusterInstance.getCluster(),
                                                                        entityManagerFactory.createEntityManager(),
                                                                        environment,
                                                                        DataSource.class,
                                                                        "DataSource"
                                                                );
                                                            }
                                                        }
                                                )
                            )
                        .type(
                                "InterestArea",
                                typeWiring ->
                                  typeWiring
                                        .dataFetcher
                                                (
                                                        "dataSources",
                                                        new DataFetcher()
                                                        {
                                                            @Override
                                                            public List<DataSource> get(DataFetchingEnvironment environment)
                                                            {
                                                                return DataRetrieverMulti.<DataSource>get(
                                                                        clusterInstance.getCluster(),
                                                                        entityManagerFactory.createEntityManager(),
                                                                        environment,
                                                                        DataSource.class,
                                                                        "DataSource"
                                                                );
                                                            }
                                                        }
                                                )
                            );

    }
}
