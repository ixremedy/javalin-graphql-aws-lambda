package com.forthreal.application.classes.model.graph;

public class VisualFeature implements IGraphQLModel
{
    public String id;
    public String title;
    public int position;
    public String linkToFormat;
    public DataSource[] dataSources;
}
