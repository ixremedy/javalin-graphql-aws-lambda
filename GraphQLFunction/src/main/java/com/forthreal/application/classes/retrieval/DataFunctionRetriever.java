package com.forthreal.application.classes.retrieval;

import javax.persistence.EntityManager;

import com.forthreal.application.classes.pojo.PojoWriter;
import graphql.schema.DataFetchingEnvironment;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import com.forthreal.application.classes.model.graph.IGraphQLModel;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.javatuples.Pair;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

import static org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource.traversal;

public class DataFunctionRetriever
{
    public static <T extends IGraphQLModel> T get
            (
              Cluster neptuneCluster,
              EntityManager auroraEntityManager,
              DataFetchingEnvironment environment,
              Class<?> functionArgumentTypeClass,
              Class<?> returnedResultTypeClass,
              String objectLabel,
              String fieldName
            )
    {

        GraphTraversalSource g = traversal().withRemote( DriverRemoteConnection.using( neptuneCluster ) );

        Optional<Vertex> opVtx = Optional.empty();

        try
        {
            /* get the arguments to find the needed object */

            if( functionArgumentTypeClass.getSimpleName().toLowerCase().contentEquals("string") == true )
            {
                String val = environment.getArgument(fieldName);
                System.out.println(">>> " + fieldName + "(string): " + val);

                opVtx = g.V().hasLabel( objectLabel ).has( fieldName,val ).tryNext();
            }
            else if( functionArgumentTypeClass.getSimpleName().toLowerCase().contentEquals("int") == true )
            {
                int val = environment.getArgument(fieldName);
                System.out.println(">>> " + fieldName + "(int): " + val);

                opVtx = g.V().hasLabel( objectLabel ).has( fieldName,val ).tryNext();
            }
            else if( functionArgumentTypeClass.getSimpleName().toLowerCase().contentEquals("long") == true )
            {
                long val = environment.getArgument(fieldName);
                System.out.println(">>> " + fieldName + "(long): " + val);

                opVtx = g.V().hasLabel( objectLabel ).has( fieldName,val ).tryNext();
            }
            else if( functionArgumentTypeClass.getSimpleName().toLowerCase().contentEquals("float") == true )
            {
                float val = environment.getArgument(fieldName);
                System.out.println(">>> " + fieldName + "(float): " + val);

                opVtx = g.V().hasLabel( objectLabel ).has( fieldName,val ).tryNext();
            }
            else if( functionArgumentTypeClass.getSimpleName().toLowerCase().contentEquals("double") == true )
            {
                double val = environment.getArgument(fieldName);
                System.out.println(">>> " + fieldName + "(double): " + val);

                opVtx = g.V().hasLabel( objectLabel ).has( fieldName,val ).tryNext();
            }
        }
        catch(Exception exc) { }

        /* not a root type query */
        if( opVtx.isEmpty() == true )
        {
            String parentObject = environment.getExecutionStepInfo().getObjectType().getName();
            Optional<String> opParentParam = environment.getExecutionStepInfo().getParent().getArguments().keySet().stream().findFirst();

            if( opParentParam.isPresent() == true )
            {
                String parentParam = opParentParam.get();
                String parentValue = environment.getExecutionStepInfo().getParent().getArgument( parentParam );
                //System.out.println("parent name: " + parentObject + " :: " + parentParam + " :: " + parentValue);

                /* find ourselves by our parent */
                opVtx = g.V().hasLabel( parentObject ).has( parentParam, parentValue ).out().V().hasLabel( objectLabel ).tryNext();
            }
            else
            {
                return null;
            }

        }

        /* in fact as result of valueMap() you get a HashMap<String,ArrayList>, where ArrayList holds the list
         * of all the values that could be saved for the requested property (it allows to save several values
         * on one property's name) */

        Optional<Map<Object,Object>> opFoundVtx = g.V( opVtx.get().id() ).valueMap().tryNext();

        /* the Vertex is found */
        if( opFoundVtx.isPresent() == true )
        {
            System.out.println(">>> " + ( (ArrayList) opFoundVtx.get().get( "title" ) ).get( 0 ) );

            List<Pair<String,String>> fieldList = PojoWriter.getPojoFields( returnedResultTypeClass );
            Constructor ctor = PojoWriter.getConstructor( returnedResultTypeClass );

            /* check if there's a constructor with which we can instantiate the POJO */
            if( ctor != null )
            {
                try
                {
                    /* make an instance of the object, the fields of which we will be
                     * populating */
                    Object pojo = ctor.newInstance();

                    /* go through the list of the fields which are requested to be returned,
                     * these values must be ones contained in the schema */
                    environment
                            .getSelectionSet()
                            .getFields()
                            .stream()
                            .forEach
                                    (
                                      field -> {
                                         String alias = field.getName();

                                         Optional<Pair<String,String>> foundField =
                                                 fieldList
                                                    .stream()
                                                    .filter( f -> f.getValue0().contentEquals(alias) )
                                                    .findFirst();

                                         if( foundField.isPresent() == true )
                                         {
                                             Field dstField =
                                                 PojoWriter.getPojoField
                                                         (
                                                           returnedResultTypeClass,
                                                           fieldName
                                                         );

                                             // System.out.print(">>> setting value for " + alias);

                                             if( opFoundVtx.get().containsKey( alias ) == true )
                                             {
                                                     Object value = ((ArrayList) opFoundVtx.get().get( alias )).get(0);

                                                     boolean writeSuccess =
                                                                 PojoWriter.writeProperty
                                                                         (
                                                                           returnedResultTypeClass,
                                                                           pojo,
                                                                           alias,
                                                                           value
                                                                         );

                                                     //System.out.println(": " + writeSuccess);
                                             }
                                             else
                                             {
                                                     System.out.print("Field \"" + alias + "\" isn't contained in the graph object");
                                             }
                                         }


                                      }
                                    );

                    return (T) pojo;
                }
                catch (Exception exc)
                {
                    exc.printStackTrace();

                    System.err.println("Caught exception in object instantiation: " + exc.getLocalizedMessage());
                }

            }
        }

        return null;

    }

}
