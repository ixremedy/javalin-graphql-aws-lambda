package com.forthreal.application.classes.db;

import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.ser.Serializers;

public class NeptuneClusterInstance
{
    private Cluster cluster;
    private String endpoint;
    private int port;
    private String certFile;

    public NeptuneClusterInstance(String endpoint, int port, String certFile)
    {
        this.endpoint = endpoint;
        this.port = port;
        this.certFile = certFile;

        Cluster.Builder builder = Cluster.build();

        builder
            .addContactPoint( endpoint )
            .port( port )
            .serializer( Serializers.GRAPHBINARY_V1D0 )
            .enableSsl( true )
            .keyCertChainFile( certFile );

        cluster = builder.create();
    }

    public Cluster getCluster()
    {
        return cluster;
    }

}
