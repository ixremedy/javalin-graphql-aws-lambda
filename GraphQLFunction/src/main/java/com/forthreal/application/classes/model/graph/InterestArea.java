package com.forthreal.application.classes.model.graph;

public class InterestArea implements IGraphQLModel
{
    public String id;
    public String title;
    public DataSource[] dataSources;
}
