package com.forthreal.application.classes.api;

import java.util.concurrent.CompletableFuture;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import io.javalin.http.Context;
import io.javalin.http.Handler;

public class Api
{
    private static GraphQL graphQLInstance = null;

    public static void create(GraphQL graphQL)
    {
        graphQLInstance = graphQL;
    }

    /** All the Web-requests coming in are served through this API class */
    public static Handler getHandler()
    {
        return
           (Context ctx) -> {
               String body = ctx.body();

               ExecutionInput input = null;

               if( body.isBlank() == true )
               {
                   String emptyQuery = "{ __schema { types { name } } }";

                   input = ExecutionInput.newExecutionInput().query( emptyQuery ).build();
               }
               else
               {
                   input = ExecutionInput.newExecutionInput().query( body ).build();
               }

               CompletableFuture<ExecutionResult> future = graphQLInstance.executeAsync( input );

               future
                   .thenAccept( result -> {

                         ctx.contentType("text/plain");

                         if( result.getData() != null )
                         {
                            // ctx.ip() + ":" + ctx.port();
                            ctx.result( result.getData().toString() );
                         }
                         else
                         {
                             ctx.result( "No data returned" );
                         }

                   })
                   .exceptionally( exc -> {
                         exc.printStackTrace();

                         System.err.println("Exception: " + exc.toString() );

                         ctx.status( 500 );
                         ctx.contentType("text/plain");
                         ctx.result( exc.toString() );

                         return null;
                   });
                };

    }

}
