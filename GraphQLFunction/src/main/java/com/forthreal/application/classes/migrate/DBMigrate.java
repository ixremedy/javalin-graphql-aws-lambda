package com.forthreal.application.classes.migrate;

import static com.forthreal.application.classes.model.relational.RDBEntities.getEntityInstance;
import static org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource.traversal;
import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal.Symbols.id;

import com.forthreal.application.classes.Tools;
import com.forthreal.application.classes.model.relational.*;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;

public class DBMigrate
{

    public static <C extends IRelationalEntity> void relationalDoTruncate(Class<C> dataClass, EntityManager manager)
    {
        CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();

        /* truncate DataType table */
        CriteriaDelete<C> typeDelete = criteriaBuilder.createCriteriaDelete(dataClass);

        try
        {
            typeDelete.from( dataClass );
            manager.createQuery( typeDelete ).executeUpdate();
        }
        catch(Exception exc)
        {
            System.out.println("Caught exception: " + exc.getLocalizedMessage() );
        }
    }

    public static void doInitialRelationalDBMigration(EntityManager manager)
    {
        manager.getTransaction().begin();
        /* truncate tables before use */
        relationalDoTruncate(DataType1.class, manager);
        relationalDoTruncate(DataType2.class, manager);
        relationalDoTruncate(DataType3.class, manager);
        relationalDoTruncate(DataType4.class, manager);
        relationalDoTruncate(DataType5.class, manager);
        relationalDoTruncate(DataType6.class, manager);
        relationalDoTruncate(DataType7.class, manager);
        relationalDoTruncate(DataType8.class, manager);
        relationalDoTruncate(DataType9.class, manager);
        relationalDoTruncate(DataType10.class, manager);
        relationalDoTruncate(DataType11.class, manager);
        manager.getTransaction().commit();

        /* set up relations: db table alias to data type*/
        RDBEntities.populateTable("table1", DataType1.class);
        RDBEntities.populateTable("table2", DataType2.class);
        RDBEntities.populateTable("table3", DataType3.class);
        RDBEntities.populateTable("table4", DataType4.class);
        RDBEntities.populateTable("table5", DataType5.class);
        RDBEntities.populateTable("table6", DataType6.class);
        RDBEntities.populateTable("table7", DataType7.class);
        RDBEntities.populateTable("table8", DataType8.class);
        RDBEntities.populateTable("table9", DataType9.class);
        RDBEntities.populateTable("table10", DataType10.class);
        RDBEntities.populateTable("table11", DataType11.class);

        manager.getTransaction().begin();
        /* populate values */
        DataType1 type1value1 = RDBEntities.getEntityInstance("table1");
        type1value1.setSecurityGroupId( (long) 1 );
        type1value1.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type1value1 );

        DataType1 type1value2 = RDBEntities.getEntityInstance("table1");
        type1value2.setSecurityGroupId( (long) 1 );
        type1value2.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type1value2 );

        DataType2 type2value1 = RDBEntities.getEntityInstance("table2");
        type2value1.setSecurityGroupId( (long) 1 );
        type2value1.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type2value1 );

        DataType2 type2value2 = RDBEntities.getEntityInstance("table2");
        type2value2.setSecurityGroupId( (long) 1 );
        type2value2.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type2value2 );

        DataType3 type3value1 = RDBEntities.getEntityInstance("table3");
        type3value1.setSecurityGroupId( (long) 1 );
        type3value1.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type3value1 );

        DataType3 type3value2 = RDBEntities.getEntityInstance("table3");
        type3value2.setSecurityGroupId( (long) 1 );
        type3value2.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type3value2 );

        DataType4 type4value1 = RDBEntities.getEntityInstance("table4");
        type4value1.setSecurityGroupId( (long) 1 );
        type4value1.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type4value1 );

        DataType4 type4value2 = RDBEntities.getEntityInstance("table4");
        type4value2.setSecurityGroupId( (long) 1 );
        type4value2.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type4value2 );

        DataType5 type5value1 = RDBEntities.getEntityInstance("table5");
        type5value1.setSecurityGroupId( (long) 1 );
        type5value1.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type5value1 );

        DataType5 type5value2 = RDBEntities.getEntityInstance("table5");
        type5value2.setSecurityGroupId( (long) 1 );
        type5value2.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type5value2 );

        DataType6 type6value1 = RDBEntities.getEntityInstance("table6");
        type6value1.setSecurityGroupId( (long) 1 );
        type6value1.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type6value1 );

        DataType6 type6value2 = RDBEntities.getEntityInstance("table6");
        type6value2.setSecurityGroupId( (long) 1 );
        type6value2.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type6value2 );

        DataType7 type7value1 = RDBEntities.getEntityInstance("table7");
        type7value1.setSecurityGroupId( (long) 1 );
        type7value1.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type7value1 );

        DataType7 type7value2 = RDBEntities.getEntityInstance("table7");
        type7value2.setSecurityGroupId( (long) 1 );
        type7value2.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type7value2 );

        DataType8 type8value1 = RDBEntities.getEntityInstance("table8");
        type8value1.setSecurityGroupId( (long) 1 );
        type8value1.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type8value1 );

        DataType8 type8value2 = RDBEntities.getEntityInstance("table8");
        type8value2.setSecurityGroupId( (long) 1 );
        type8value2.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type8value2 );

        DataType9 type9value1 = RDBEntities.getEntityInstance("table9");
        type9value1.setSecurityGroupId( (long) 1 );
        type9value1.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type9value1 );

        DataType9 type9value2 = RDBEntities.getEntityInstance("table9");
        type9value2.setSecurityGroupId( (long) 1 );
        type9value2.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type9value2 );

        DataType10 type10value1 = RDBEntities.getEntityInstance("table10");
        type10value1.setSecurityGroupId( (long) 1 );
        type10value1.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type10value1 );

        DataType10 type10value2 = RDBEntities.getEntityInstance("table10");
        type10value2.setSecurityGroupId( (long) 1 );
        type10value2.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type10value2 );

        DataType11 type11value1 = RDBEntities.getEntityInstance("table11");
        type11value1.setSecurityGroupId( (long) 1 );
        type11value1.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type11value1 );

        DataType11 type11value2 = RDBEntities.getEntityInstance("table11");
        type11value2.setSecurityGroupId( (long) 1 );
        type11value2.setValue(Tools.floatBetween(1000,2000));
        manager.persist( type11value2 );

        manager.getTransaction().commit();
    }

    public static void doInitialGraphDBMigration(Cluster cluster)
    {
        GraphTraversalSource g = traversal().withRemote( DriverRemoteConnection.using( cluster ) );

        /**
         * delete all records: iteration doesn't occur automatically after a drop
         ***/
        g.V().drop().iterate();

        /* The top level entry - Page */
        g.addV("Page")
           .property("id", "285")
           .property("title","EntryPage")
           .property("url","/pages/dailySales/Ceftriaxone")
           .next();

        /* View-related stuff */
        g.addV("View").property("id", "3497").property("title","View").next();
        g.V().has("id","3497").as("higher").V().has("id","285").addE("connected").to("higher").next();

        /* the Left-most column */
        g.addV("Column")
           .property("id", "15397")
           .property("position", 1)
           .property("title", "MTD")
           .property("format", "view::format::1")
           .next();
        g.V().has("id","15397").as("higher").V().has("id","3497").addE("connected").to("higher").next();

        /* the second column */
        g.addV("Column")
                .property("id", "937")
                .property("position", 2)
                .property("title", "vs BUD")
                .next();
        g.V().has("id","937").as("higher").V().has("id","3497").addE("connected").to("higher").next();

        /* Value switchers connected to the second column */
        g.addV("VisualFeature")
                .property("id", "927")
                .property("position", 1)
                .property("title","VisualFeature")
                .property("format", "view::format::1")
                .next();
        g.V().has("id","927").as("higher").V().has("id","937").addE("connected").to("higher").next();

        g.addV("VisualFeature")
                .property("id", "2984")
                .property("position", 2)
                .property("title","VisualFeature")
                .property("format", "view::format::2")
                .next();
        g.V().has("id","2984").as("higher").V().has("id","937").addE("connected").to("higher").next();

        /* the third column */
        g.addV("Column")
                .property("id", "950")
                .property("position", 3)
                .property("title", "vs PY")
                .next();
        g.V().has("id","950").as("higher").V().has("id","3497").addE("connected").to("higher").next();

        /* Value switchers connected to the third column */
        g.addV("VisualFeature")
                .property("id", "6034")
                .property("position", 1)
                .property("title","VisualFeature")
                .property("format", "view::format::1")
                .next();
        g.V().has("id","6034").as("higher").V().has("id","950").addE("connected").to("higher").next();

        g.addV("VisualFeature")
                .property("id", "9472")
                .property("position", 2)
                .property("title","VisualFeature")
                .property("format", "view::format::2")
                .next();
        g.V().has("id","9472").as("higher").V().has("id","950").addE("connected").to("higher").next();

        /* the forth column */
        g.addV("Column")
                .property("id", "238")
                .property("position", 4)
                .property("title", "% Achieved")
                .property("format", "view::format::1")
                .next();
        g.V().has("id","238").as("higher").V().has("id","3497").addE("connected").to("higher").next();

        /* the forth column */
        g.addV("Column")
                .property("id", "2374")
                .property("position", 5)
                .property("title", "% Target")
                .property("format", "view::format::1")
                .next();
        g.V().has("id","2374").as("higher").V().has("id","3497").addE("connected").to("higher").next();

        /* the fifth column */
        g.addV("Column")
                .property("id", "9428")
                .property("position", 6)
                .property("title", "Month")
                .property("format", "view::format::1")
                .next();
        g.V().has("id","9428").as("higher").V().has("id","3497").addE("connected").to("higher").next();

        /* InterestArea-related stuff */
        g.addV("InterestArea")
           .property("id", "408")
           .property("title", "Ceftriaxone")
           .next();
        g.V().has("id","408").as("higher").V().has("id","285").addE("connected").to("higher").next();

        /* a datasource */
        g.addV("DataSource")
                .property("id", "7539")
                .property("relationalTable", "table1")
                .property("title","DataSource")
                .property("dataGranularity", "data::granularity::1")
                .property("dataSourcing", "data::source::value")
                .next();
        g.V().has("id","7539").as("higher").V().has("id","408").addE("connected").to("higher").next();
        /* connect to column 2 VisualFeature 1 */
        g.V().has("id","927").as("higher").V().has("id","7539").addE("retrievesThrough").to("higher").next();

        /* a datasource */
        g.addV("DataSource")
                .property("id", "9139")
                .property("relationalTable", "table2")
                .property("title","DataSource")
                .property("dataGranularity", "data::granularity::1")
                .property("dataSourcing", "data::source::value")
                .next();
        g.V().has("id","9139").as("higher").V().has("id","408").addE("connected").to("higher").next();
        /* connect to column 2 VisualFeature 2 */
        g.V().has("id","2984").as("higher").V().has("id","9139").addE("retrievesThrough").to("higher").next();

        /* a datasource */
        g.addV("DataSource")
                .property("id", "8360")
                .property("relationalTable", "table3")
                .property("title","DataSource")
                .property("dataGranularity", "data::granularity::1")
                .property("dataSourcing", "data::source::sum")
                .next();
        g.V().has("id","8360").as("higher").V().has("id","408").addE("connected").to("higher").next();
        /* connect to column 1 */
        g.V().has("id","15397").as("higher").V().has("id","8360").addE("retrievesThrough").to("higher").next();

        /* a datasource */
        g.addV("DataSource")
                .property("id", "1655")
                .property("relationalTable", "table5")
                .property("title","DataSource")
                .property("dataGranularity", "data::granularity::2")
                .property("dataSourcing", "data::source::value")
                .next();
        g.V().has("id","1655").as("higher").V().has("id","408").addE("connected").to("higher").next();
        /* connect to column 3 VisualFeature 1 */
        g.V().has("id","6034").as("higher").V().has("id","1655").addE("retrievesThrough").to("higher").next();

        /* a datasource */
        g.addV("DataSource")
                .property("id", "5729")
                .property("relationalTable", "table6")
                .property("title","DataSource")
                .property("dataGranularity", "data::granularity::1")
                .property("dataSourcing", "data::source::value")
                .next();
        g.V().has("id","5729").as("higher").V().has("id","408").addE("connected").to("higher").next();
        /* connect to column 3 VisualFeature 2 */
        g.V().has("id","9472").as("higher").V().has("id","5729").addE("retrievesThrough").to("higher").next();

        /* a datasource */
        g.addV("DataSource")
                .property("id", "9537")
                .property("relationalTable", "table7")
                .property("title","DataSource")
                .property("dataGranularity", "data::granularity::2")
                .property("dataSourcing", "data::source::sum")
                .next();
        g.V().has("id","9537").as("higher").V().has("id","408").addE("connected").to("higher").next();
        /* connect to column 4 */
        g.V().has("id","238").as("higher").V().has("id","9537").addE("retrievesThrough").to("higher").next();

        /* a datasource */
        g.addV("DataSource")
                .property("id", "2956")
                .property("relationalTable", "table8")
                .property("title","DataSource")
                .property("dataGranularity", "data::granularity::2")
                .property("dataSourcing", "data::source::value")
                .next();
        g.V().has("id","2956").as("higher").V().has("id","408").addE("connected").to("higher").next();
        /* connect to column 4 */
        g.V().has("id","2374").as("higher").V().has("id","2956").addE("retrievesThrough").to("higher").next();

        /* a datasource */
        g.addV("DataSource")
                .property("id", "77483")
                .property("relationalTable", "table9")
                .property("title","DataSource")
                .property("dataGranularity", "data::granularity::1")
                .property("dataSourcing", "data::source::value")
                .next();
        g.V().has("id","77483").as("higher").V().has("id","408").addE("connected").to("higher").next();
        /* connect to column 5 */
        g.V().has("id","9428").as("higher").V().has("id","77483").addE("retrievesThrough").to("higher").next();

        /* a datasource */
        g.addV("DataSource")
                .property("id", "77850")
                .property("relationalTable", "table10")
                .property("title","DataSource")
                .property("dataGranularity", "data::granularity::2")
                .property("dataSourcing", "data::source::value")
                .next();
        g.V().has("id","77850").as("higher").V().has("id","408").addE("connected").to("higher").next();

        /* a datasource */
        g.addV("DataSource")
                .property("id", "95034")
                .property("relationalTable", "table11")
                .property("title","DataSource")
                .property("dataGranularity", "data::granularity::3")
                .property("dataSourcing", "data::source::value")
                .next();
        g.V().has("id","95034").as("higher").V().has("id","408").addE("connected").to("higher").next();

        /**
         * If we wanted to see the values
         */

        GraphTraversal t = g.V().has( id );

        t.forEachRemaining(
                val -> {

                    Vertex entry = (Vertex) val;

                    System.out.println("Vertex: " + entry.label());

                    /** get all properties and values
                     * limit the properties to one Vertex only
                     * (for which we are called)
                     ***/
                    g.V( ((Vertex) val).id() )
                        .valueMap( )
                        .toList()
                        .stream()
                        .forEach(
                            map -> {
                                map.forEach(
                                        (k,v) -> {
                                           System.out.println("k: " + k + " v: " + v );
                                        }
                                );
                            }
                         );

                }
        );

    }
}
