package com.forthreal.application.classes.retrieval;

import com.forthreal.application.classes.Holder;
import com.forthreal.application.classes.model.graph.*;
import com.forthreal.application.classes.model.relational.IRelationalEntity;
import com.forthreal.application.classes.model.relational.RDBEntities;
import com.forthreal.application.classes.pojo.PojoWriter;
import graphql.execution.ExecutionStepInfo;
import graphql.schema.DataFetchingEnvironment;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.javatuples.Pair;

import javax.persistence.EntityManager;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

import static org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource.traversal;
import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.*;

public class DataRetrieverMulti
{
    public static <T extends IGraphQLModel> List<T> get
            (
                    Cluster neptuneCluster,
                    EntityManager auroraEntityManager,
                    DataFetchingEnvironment environment,
                    Class<?> returnedResultTypeClass,
                    String objectLabel
            )
    {
        System.out.println("Fetching data for " + objectLabel );

        ExecutionStepInfo currentStep = environment.getExecutionStepInfo();
        String parentObject = currentStep.getObjectType().getName();

        System.out.println("parent name: " + parentObject );

        Optional<String> opParentParam = currentStep.getParent().getArguments().keySet().stream().findFirst();

        if( opParentParam.isEmpty() )
        {
            return List.of();
        }

        String parentParam = opParentParam.get();
        String parentValue = environment.getExecutionStepInfo().getParent().getArgument( parentParam );
        //System.out.println("parent name: " + parentObject + " :: " + parentParam + " :: " + parentValue);

        GraphTraversalSource g = traversal().withRemote( DriverRemoteConnection.using( neptuneCluster ) );

        /* find ourselves by our parent */
        Optional<Vertex> opVtx =
                g.V()
                   .hasLabel( parentObject )
                   .has( parentParam, parentValue )
                   .out().hasLabel( objectLabel )
                   .tryNext();

        if( opVtx.isPresent() == true )
        {

            List<Pair<String,String>> fieldList = PojoWriter.getPojoFields( returnedResultTypeClass );
            Constructor ctor = PojoWriter.getConstructor( returnedResultTypeClass );

            /* check if there's a constructor with which we can instantiate the POJO */
            if( ctor != null )
            {
                List<T> objects = new ArrayList<>();

                GraphTraversal t =
                        g.V()
                            .has( parentObject, parentParam, parentValue )
                            .out()
                            .hasLabel( objectLabel );

                System.out.println("Object: " + objectLabel);

                EntityManager manager = auroraEntityManager.getEntityManagerFactory().createEntityManager();

                while ( t.hasNext() == true )
                {

                    Vertex leaf = (Vertex) t.next();
                    Optional<Map<Object,Object>> opFoundVtx = g.V( leaf.id() ).valueMap().tryNext();

                    //System.out.println("Label: " + leaf.label());

                    try {

                        /* make an instance of the object, the fields of which we will be
                         * populating */
                        Object pojo = ctor.newInstance();

                        /* go through the list of the fields which are requested to be returned,
                         * these values must be ones contained in the schema */
                        environment
                                .getSelectionSet()
                                .getFields()
                                .stream()
                                .forEach
                                        (
                                                field -> {
                                                    String alias = field.getName();

                                                    //System.out.println(">>> alias " + alias );

                                                    Optional<Pair<String, String>> foundField =
                                                            fieldList
                                                                    .stream()
                                                                    .filter(f -> f.getValue0().contentEquals(alias))
                                                                    .findFirst();

                                                    if (foundField.isPresent() == true)
                                                    {

                                                        Field dstField =
                                                                PojoWriter.getPojoField
                                                                        (
                                                                                returnedResultTypeClass,
                                                                                alias
                                                                        );

                                                        //System.out.println(">>> field: " + alias);

                                                        /* specific realisation of retrieval of data */
                                                        if( ( dstField != null ) && ( alias.contentEquals("value") == true ) )
                                                        {
                                                            /* the value is to be taken from the table provided in this field */
                                                            String relational =
                                                                    (String)
                                                                      ((ArrayList) opFoundVtx.get().get( "relationalTable" )).get(0);

                                                            if( relational != null )
                                                            {
                                                                boolean sum = false;

                                                                Optional<Class<IRelationalEntity>> opClass =
                                                                        RDBEntities.getClassByEntityName( relational );

                                                                String sourceType =
                                                                        (String)
                                                                                ((ArrayList) opFoundVtx.get().get( "dataSourcing" )).get(0);

                                                                if( ( sourceType.isBlank() == false )
                                                                    && ( sourceType.endsWith("::sum") == true ) )
                                                                {
                                                                    sum = true;
                                                                }


                                                                if( opClass.isPresent() == true )
                                                                {

                                                                    if( sum == false )
                                                                    {
                                                                        Optional<Object> opValue =
                                                                            RDBEntities.getEntityLastValue
                                                                                (
                                                                                        manager,
                                                                                        relational
                                                                                );

                                                                        if( opValue.isPresent() == true )
                                                                        {
                                                                            Object value =
                                                                                    PojoWriter.readProperty( opClass.get(), opValue.get(), "value" );

                                                                            if( value != null )
                                                                            {
                                                                                boolean writeSuccess =
                                                                                        PojoWriter.writeProperty
                                                                                                (
                                                                                                        returnedResultTypeClass,
                                                                                                        pojo,
                                                                                                        alias,
                                                                                                        value
                                                                                                );

                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        System.out.println(">>> fsdfjhldfhsdklfjhsdkfjhsdkfjhdskfjsh");

                                                                        Optional<Object> opValue =
                                                                                RDBEntities.getEntitySumValue
                                                                                        (
                                                                                                manager,
                                                                                                relational
                                                                                        );

                                                                        Optional<Class<IRelationalEntity>> dstObjectClass =
                                                                                RDBEntities.getClassByEntityName
                                                                                        (
                                                                                                relational
                                                                                        );

                                                                        if( ( opValue.isPresent() == true ) && (dstObjectClass.isPresent() == true) )
                                                                        {

                                                                            Double intermediate = (Double) opValue.get();

                                                                            boolean writeSuccess =
                                                                                  PojoWriter.writeProperty
                                                                                       (
                                                                                          returnedResultTypeClass,
                                                                                          pojo,
                                                                                          alias,
                                                                                          intermediate.floatValue()
                                                                                       );

                                                                        }

                                                                    }
                                                                }


                                                            }

                                                        }
                                                        else if( opFoundVtx.get().containsKey( alias ) == true )
                                                        {
                                                            Object value = ((ArrayList) opFoundVtx.get().get( alias )).get(0);

                                                            boolean writeSuccess =
                                                                    PojoWriter.writeProperty
                                                                            (
                                                                                    returnedResultTypeClass,
                                                                                    pojo,
                                                                                    alias,
                                                                                    value
                                                                            );

                                                        }
                                                        else
                                                        {
                                                            //System.out.print("Field \"" + alias + "\" isn't contained in the graph object");
                                                        }
                                                    }


                                                }
                                        );

                        objects.add( (T) pojo );
                    } catch (Exception exc) {
                        exc.printStackTrace();

                        System.err.println("Caught exception in object instantiation: " + exc.getLocalizedMessage());
                    }
                }

                return objects;

            }
        }

        return List.of();

    }
}
