package com.forthreal.application.classes.model.graph;

public class DataSource implements IGraphQLModel
{
    public String id;
    public String title;
    public String relationalTable;
    public String dataGranularity;
    public String dataSourcing;
    public float value;
    public Column column;
    public VisualFeature visualFeature;
}
