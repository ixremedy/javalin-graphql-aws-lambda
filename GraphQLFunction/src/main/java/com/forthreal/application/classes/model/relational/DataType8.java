package com.forthreal.application.classes.model.relational;

import javax.persistence.*;

@Entity
@Table(name = "table8", indexes = { @Index(columnList = "columnId") } )
public class DataType8 implements IRelationalEntity
{
    @Id
    @GeneratedValue
    protected long id;

    protected String columnId;

    protected long securityGroupId;

    protected float value;

    public long getId()
    {
        return id;
    }

    public long getSecurityGroupId()
    {
        return securityGroupId;
    }

    public void setSecurityGroupId(long securityGroupId)
    {
        this.securityGroupId = securityGroupId;
    }

    public float getValue()
    {
        return value;
    }

    public void setValue(float value)
    {
        this.value = value;
    }

    public String getColumnId()
    {
        return columnId;
    }

    public void setColumnId(String columnId)
    {
        this.columnId = columnId;
    }
}

