package com.forthreal.application.classes.model.graph;

public class Column implements IGraphQLModel
{
    public String id;
    public String title;
    public int position;
    public VisualFeature[] visualFeatures;
    public DataSource[] dataSources;
}
