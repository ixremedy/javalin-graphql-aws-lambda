package com.forthreal.application.classes;

public class OverrideThrowChecking
{
    public static <T extends Throwable> void doThrow(Throwable exception) throws T
    {
        throw (T) exception;
    }
}
