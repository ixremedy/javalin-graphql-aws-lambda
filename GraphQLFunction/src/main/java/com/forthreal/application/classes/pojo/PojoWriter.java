package com.forthreal.application.classes.pojo;

import com.forthreal.application.classes.OverrideThrowChecking;
import org.javatuples.Pair;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PojoWriter
{
    public static <T> Object readProperty(Class<T> cls, Object pojo, String fieldName)
    {
        List<Pair<String,String>> fields = getPojoFields( cls );

        Optional<Pair<String,String>> foundField =
                fields
                        .stream()
                        .filter( f -> f.getValue0().contentEquals( fieldName ) )
                        .findFirst();

        if(foundField.isPresent() == true)
        {
            /* should not be null - we checked earlier */
            boolean canRead = false;
            Field field = getPojoField( cls, fieldName );

            System.out.println("cls.name: " + cls.getSimpleName() + " object type: " + pojo.getClass().getSimpleName());

            if( field.canAccess( pojo ) == false )
            {
                if( field.trySetAccessible() == true )
                {
                    canRead = true;
                }
            }
            else
            {
                canRead = true;
            }

            if(canRead == true)
            {
                try
                {
                    if( field.getType().getSimpleName().toLowerCase().contentEquals("long") == true )
                    {
                        long value = field.getLong( pojo );
                        return value;
                    }
                    else if( field.getType().getSimpleName().toLowerCase().startsWith("int") == true )
                    {
                        int value = field.getInt( pojo );
                        return value;
                    }
                    else if( field.getType().equals( String.class ) == true )
                    {
                        String value = (String) field.get( pojo );
                        return value;
                    }
                    else if( field.getType().getSimpleName().toLowerCase().contentEquals("float") == true )
                    {
                        float value = field.getFloat( pojo );
                        return value;
                    }
                    else if( field.getType().getSimpleName().toLowerCase().contentEquals("double") == true )
                    {
                        double value = field.getDouble( pojo );
                        return value;
                    }

                    System.err.println("Couldn't identify value type: " + field.getType().getName().toLowerCase());
                }
                catch(Exception exc)
                {
                    System.err.println("Can't read value: " + exc.getLocalizedMessage());
                }

            }
        }

        return null;
    }

    public static <T> boolean writeProperty(Class<T> cls, Object pojo, String fieldName, Object value)
    {
        List<Pair<String,String>> fields = getPojoFields( cls );

        Optional<Pair<String,String>> foundField =
                fields
                        .stream()
                        .filter( f -> f.getValue0().contentEquals( fieldName ) )
                        .findFirst();

        if(foundField.isPresent() == true)
        {
            /* should not be null - we checked earlier */
            boolean canWrite = false;
            Field field = getPojoField( cls, fieldName );

            if( field.canAccess( pojo ) == false )
            {
                if( field.trySetAccessible() == true )
                {
                    canWrite = true;
                }
            }
            else
            {
                canWrite = true;
            }

            if(canWrite == true)
            {
                /*
                System.out.println
                    (
                      "setting "
                        + fieldName
                        + " "
                        + field.getType().getName()
                        + " " + value
                    );
                 */

                try
                {
                    if( field.getType().getName().toLowerCase().contentEquals("long") == true )
                    {
                        field.setLong( pojo, (Long ) value );
                        return true;
                    }
                    else if( field.getType().getName().toLowerCase().startsWith("int") == true )
                    {
                        field.setInt( pojo, (Integer) value );
                        return true;
                    }
                    else if( field.getType().equals( String.class ) == true )
                    {
                        field.set( pojo, value );
                        return true;
                    }
                    else if( field.getType().getName().toLowerCase().contentEquals("float") == true )
                    {
                        field.setFloat( pojo, (Float) value );
                        return true;
                    }
                    else if( field.getType().getName().toLowerCase().contentEquals("double") == true )
                    {
                        field.setDouble( pojo, (Double) value );
                        return true;
                    }
                }
                catch(Exception exc)
                {
                    System.err.println("Can't set value: " + exc.getLocalizedMessage());
                }
            }
            else
            {
                System.out.println("Field " + fieldName + " is not accessible");
            }
        }
        else
        {
            System.err.println("Unable to find field: " + fieldName);
        }

        return false;
    }

    public static <T> Constructor<T> getConstructor(Class<T> cls)
    {
        for( Constructor ctor: cls.getDeclaredConstructors() )
        {
            if( ctor.getGenericParameterTypes().length == 0 )
            {
                return ctor;
            }
        }

        return null;
    }

    public static <T> Field getPojoField(Class<T> cls, String fieldName)
    {
        try
        {
            return cls.getField( fieldName );
        }
        catch( Exception exc ) {}

        try
        {
            return cls.getDeclaredField( fieldName );
        }
        catch( Exception exc ) {}

        return null;
    }

    public static <T> List<Pair<String,String>> getPojoFields(Class<T> cls)
    {
        List<Pair<String,String>> fields = new ArrayList<>();

        for( Field field: cls.getFields() )
        {
            fields.add( Pair.with(field.getName(), field.getType().getName()) );
        }

        for( Field field: cls.getDeclaredFields() )
        {
            fields.add( Pair.with(field.getName(), field.getType().getName()) );
        }

        return fields;
    }
}
