package com.forthreal.application.classes.db;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import static javax.persistence.Persistence.createEntityManagerFactory;

public class AuroraEntityManagerInstance
{
    private static EntityManagerFactory entityManagerFactory = null;

    public static EntityManagerFactory getFactory()
    {
        if( entityManagerFactory == null )
        {
            entityManagerFactory = createEntityManagerFactory("AuroraPostgresql");
        }

        return entityManagerFactory;
    }

    public static EntityManager getInstance()
    {
        if( entityManagerFactory == null )
        {
            entityManagerFactory = createEntityManagerFactory("AuroraPostgresql");
        }

        return entityManagerFactory.createEntityManager();
    }
}
