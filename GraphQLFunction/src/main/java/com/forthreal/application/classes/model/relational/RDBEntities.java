package com.forthreal.application.classes.model.relational;

import com.forthreal.application.classes.pojo.PojoWriter;

import javax.persistence.EntityManager;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class RDBEntities<T extends IRelationalEntity>
{
    private static RDBEntities dbEntities = new RDBEntities();
    protected Map<String,Class<T>> entityTable = new HashMap<>();

    public static <T extends IRelationalEntity> void populateTable(String entityName, Class<T> entityClass)
    {
        if( dbEntities.entityTable.containsKey( entityName ) == true )
        {
            dbEntities.entityTable.remove( entityName );
        }

        dbEntities.entityTable.put( entityName, entityClass );
    }

    public static <T extends IRelationalEntity> Optional<Class<T>> getClassByEntityName( String entityName )
    {
        if( dbEntities.entityTable.containsKey( entityName ) == true )
        {
            return Optional.of( (Class<T>) dbEntities.entityTable.get( entityName ) );
        }

        return Optional.empty();
    }

    public static <T extends IRelationalEntity> T getEntityInstance( String entityName )
    {
        Optional<Class<T>> opCls = getClassByEntityName( entityName );

        if( opCls.isPresent() == true )
        {
            Constructor ctor = PojoWriter.getConstructor( opCls.get() );

            if( ctor != null )
            {
                try
                {
                    Object pojo = ctor.newInstance();

                    return (T) pojo;
                }
                catch (Exception exc) { }
            }
        }

        return null;
    }

    public static <T extends IRelationalEntity>
        Optional<Object>
        getEntitySumValue(EntityManager manager, String entityName)
    {
        Optional<Object> opValue = Optional.empty();

        Object instance = getEntityInstance( entityName );
        Optional<Class<T>> opClass = getClassByEntityName( entityName );

        if( ( instance != null ) && ( opClass.isPresent() == true ) )
        {
            Field field = PojoWriter.getPojoField( opClass.get(), "value" );

            /* field is found */
            if( field != null )
            {
                Object result =
                        manager.createQuery
                                (
                                        "SELECT SUM(value) AS value from "
                                                  + opClass.get().getSimpleName()
                                                  + " GROUP BY securityGroupId "
                                )
                                .setMaxResults( 1 )
                                .getSingleResult();

                opValue = Optional.of( result );
            }
        }

        return opValue;
    }


    public static <T extends IRelationalEntity>
        Optional<Object>
        getEntityLastValue(EntityManager manager, String entityName)
    {
        Optional<Object> opValue = Optional.empty();

        Object instance = getEntityInstance( entityName );
        Optional<Class<T>> opClass = getClassByEntityName( entityName );

        if( ( instance != null ) && ( opClass.isPresent() == true ) )
        {
            Field field = PojoWriter.getPojoField( opClass.get(), "value" );

            /* field is found */
            if( field != null )
            {
                T result =
                        manager.createQuery
                            (
                               "from " + opClass.get().getSimpleName() + " ORDER BY id DESC",
                               opClass.get()
                            )
                                .setMaxResults( 1 )
                                .getSingleResult();

                opValue = Optional.of( result );
            }
        }

        return opValue;
    }
}
