package com.forthreal.application.classes;

public class Holder<T>
{
    public T value;

    public T getValue()
    {
        return value;
    }

    public void setValue(T value)
    {
        this.value = value;
    }
}
