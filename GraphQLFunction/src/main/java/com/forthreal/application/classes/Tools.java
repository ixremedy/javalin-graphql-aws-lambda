package com.forthreal.application.classes;

import java.util.Random;

public class Tools
{
    public static float floatBetween(int min, int max)
    {
        java.util.Random rand = new java.util.Random();
        float f1 = rand.nextFloat();

        return ( ( (float)( max - min ) ) * f1 ) + (float) min;
    }
}
