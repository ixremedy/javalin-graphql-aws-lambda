package com.forthreal.application.classes.retrieval;

import com.forthreal.application.classes.Holder;
import com.forthreal.application.classes.model.graph.*;
import graphql.schema.DataFetchingEnvironment;
import org.apache.tinkerpop.gremlin.driver.Cluster;

import javax.persistence.EntityManager;

public class DataRetrieverSingle
{
    public static <T extends IGraphQLModel> T get
            (
              Cluster neptuneCluster,
              EntityManager auroraEntityManager,
              DataFetchingEnvironment environment,
              Class<?> functionArgumentTypeClass,
              Class<?> returnedResultTypeClass,
              String objectLabel,
              String fieldName
            )
    {
        System.out.println("Fetching data for " + objectLabel + ":" + fieldName );

        environment.getArguments().forEach(
          (k,v) -> {
             System.out.println(">>>> " + k + " :: " + v);
          }
        );

        Holder<T> foundObject = new Holder<>();
        foundObject.value = null;

        if( returnedResultTypeClass.equals(InterestArea.class) == true )
        {
            InterestArea area = new InterestArea();
            foundObject.value = (T) area;
        }
        else if( returnedResultTypeClass.equals(View.class) == true )
        {
            View view = new View();
            foundObject.value = (T) view;
        }
        else if( returnedResultTypeClass.equals(Column.class) == true )
        {
            Column column = new Column();
            foundObject.value = (T) column;
        }
        else if( returnedResultTypeClass.equals(VisualFeature.class) == true )
        {
            VisualFeature feature = new VisualFeature();
            foundObject.value = (T) feature;
        }


            /* go through the list of the fields which are requested to be returned,
         * these values must be ones contained in the schema */
        environment
                .getSelectionSet()
                .getFields()
                .stream()
                .forEach
                        (
                           field -> {

                               if( returnedResultTypeClass.equals(InterestArea.class) == true )
                               {
                                   InterestArea area = (InterestArea) foundObject.value;
                                   area.id = "123";
                               }
                               else if( returnedResultTypeClass.equals(View.class) == true )
                               {
                                   View view = (View) foundObject.value;
                                   view.id = "234";
                               }
                               else if( returnedResultTypeClass.equals(Column.class) == true )
                               {
                                   Column column = (Column) foundObject.value;
                                   column.id = "345";
                               }
                               else if( returnedResultTypeClass.equals(VisualFeature.class) == true )
                               {
                                   VisualFeature feature = (VisualFeature) foundObject.value;
                                   feature.id = "456";
                               }


                               String alias = field.getName();

                               System.out.println("alias: " + alias);
                           });

        return foundObject.value;

    }

}
