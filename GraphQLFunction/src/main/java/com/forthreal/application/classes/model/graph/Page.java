package com.forthreal.application.classes.model.graph;

public class Page implements IGraphQLModel
{
    public String id;
    public String title;
    public String uri;
    public View view;
    public InterestArea interestArea;
}
