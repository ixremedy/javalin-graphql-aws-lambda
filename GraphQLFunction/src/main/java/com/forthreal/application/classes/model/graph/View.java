package com.forthreal.application.classes.model.graph;

public class View implements IGraphQLModel
{
    public String id;
    public String title;
    public Column[] columns;
}
